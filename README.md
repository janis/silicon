# Silicon Dark

Silicon is a beautiful, minimalist dark theme. Carefully selected shades help organize the editors parts without distracting you from what's really important: Your code.

The code highlights colors are based on Atom One Dark. The editor is kept in warm-dark gray shades, friendly, but staying out of your way.

## Screenshots

![Screenshot 1](https://gitlab.com/janis/silicon/-/raw/main/static/Silicon_dark_screenshot_1.png)

![Screenshot 2](https://gitlab.com/janis/silicon/-/raw/main/static/Silicon_dark_screenshot_2.png)

## Issues

For any issue with this theme, as well as for any suggestions, feel free to open an issue in the [GitLab Repo](https://gitlab.com/janis/silicon/-/issues)
